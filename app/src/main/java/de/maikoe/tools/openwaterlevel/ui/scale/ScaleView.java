/*
 * Copyright (c) 2019 Bernd Jens Maier. This file is part of OpenWaterLevel.
 *
 * OpenWaterLevel is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * OpenWaterLevel is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with OpenWaterLevel.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package de.maikoe.tools.OpenWaterLevel.ui.scale;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.AttributeSet;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;

import java.util.Locale;

import de.maikoe.tools.OpenWaterLevel.R;

/**
 * Custom View class which draws the scale graphics for the scale fragment.
 */
public class ScaleView extends View implements Observer<float[]> {

    private final int BALL_SIZE = 2;
    private final int ANGLE_SCALING = 10;

    private final ShapeDrawable bubble;
    private final Paint paint;
    private final Point bubbleCenter;
    private final Point viewCenter;
    private float rotXdegree, rotYdegree;

    public ScaleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        paint = new Paint();
        paint.setAntiAlias(true);
        bubble = new ShapeDrawable(new OvalShape());
        bubbleCenter = new Point();
        viewCenter = new Point();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //background
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.colorBackground));
        canvas.drawPaint(paint);
        //angles as text
        paint.setColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(this.getHeight() / 25);
        canvas.drawText(String.format(Locale.getDefault(), "Angles [°]: %3.1f, %3.1f", rotXdegree, rotYdegree), this.getWidth() / 2, this.getHeight() - paint.getTextSize(), paint);
        paint.setTextAlign(Paint.Align.LEFT);
        for (int i = 0; i < 4; i++) {
            canvas.save();
            canvas.rotate(90 * i, viewCenter.x, viewCenter.y);
            canvas.drawText("5°", viewCenter.x + 10, viewCenter.y - 6 * ANGLE_SCALING, paint);
            canvas.drawText("10°", viewCenter.x + 10, viewCenter.y - 11 * ANGLE_SCALING, paint);
            canvas.drawText("15°", viewCenter.x + 10, viewCenter.y - 16 * ANGLE_SCALING, paint);
            canvas.drawText("20°", viewCenter.x + 10, viewCenter.y - 21 * ANGLE_SCALING, paint);
            canvas.restore();
        }
        //crosshair
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        canvas.drawCircle(viewCenter.x, viewCenter.y, BALL_SIZE * ANGLE_SCALING + 2, paint);
        canvas.drawCircle(viewCenter.x, viewCenter.y, 5 * ANGLE_SCALING, paint);
        canvas.drawCircle(viewCenter.x, viewCenter.y, 10 * ANGLE_SCALING, paint);
        canvas.drawCircle(viewCenter.x, viewCenter.y, 15 * ANGLE_SCALING, paint);
        canvas.drawCircle(viewCenter.x, viewCenter.y, 20 * ANGLE_SCALING, paint);
        canvas.drawLine(viewCenter.x - 25 * ANGLE_SCALING, viewCenter.y, viewCenter.x + 25 * ANGLE_SCALING, viewCenter.y, paint);
        canvas.drawLine(viewCenter.x, viewCenter.y - 25 * ANGLE_SCALING, viewCenter.x, viewCenter.y + 25 * ANGLE_SCALING, paint);
        //bubble
        bubble.draw(canvas);
    }

    @Override
    public void onChanged(float[] changedAngles) {
        rotXdegree = (float) Math.toDegrees(changedAngles[2]);
        rotYdegree = (float) Math.toDegrees(changedAngles[1]);
        viewCenter.x = this.getWidth() / 2;
        viewCenter.y = this.getHeight() / 2;
        updateBubble();
        invalidate();
    }

    private void updateBubble() {
        bubbleCenter.x = this.getWidth() / 2;
        bubbleCenter.y = this.getHeight() / 2;
        bubbleCenter.offset(Math.round(rotXdegree) * ANGLE_SCALING, Math.round(rotYdegree) * ANGLE_SCALING);
        bubble.setBounds(bubbleCenter.x - BALL_SIZE * ANGLE_SCALING, bubbleCenter.y - BALL_SIZE * ANGLE_SCALING, bubbleCenter.x + BALL_SIZE * ANGLE_SCALING, bubbleCenter.y + BALL_SIZE * ANGLE_SCALING);
        if (Math.abs(rotXdegree) < 1.05 && Math.abs(rotYdegree) < 1.05) {
            bubble.getPaint().setColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        } else {
            bubble.getPaint().setColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        }
    }
}
