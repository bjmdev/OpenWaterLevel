/*
 * Copyright (c) 2019 Bernd Jens Maier. This file is part of OpenWaterLevel.
 *
 * OpenWaterLevel is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * OpenWaterLevel is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with OpenWaterLevel.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package de.maikoe.tools.OpenWaterLevel.ui.scale;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 * Provides sensor data, optionally calibrated, to ScaleFragment.
 */
public class ScaleViewModel extends ViewModel implements SensorEventListener {

    final MutableLiveData<float[]> currentAngles = new MutableLiveData<>();
    private final float[] rotationMatrix = new float[9];
    private final float[] orientationAngles = new float[3];
    private final float[] orientationCalibration = new float[3];
    private final float[] returnedAngles = new float[3];

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // You must implement this callback in your code.
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_GAME_ROTATION_VECTOR) {
            SensorManager.getRotationMatrixFromVector(rotationMatrix, event.values);
            SensorManager.getOrientation(rotationMatrix, orientationAngles);
            returnedAngles[0] = orientationAngles[0] - orientationCalibration[0];
            returnedAngles[1] = orientationAngles[1] - orientationCalibration[1];
            returnedAngles[2] = orientationAngles[2] - orientationCalibration[2];
            currentAngles.setValue(returnedAngles);
        }
    }

    /**
     * Calibrates current orientation angles to zero
     */
    void calibrate() {
        orientationCalibration[0] = orientationAngles[0];
        orientationCalibration[1] = orientationAngles[1];
        orientationCalibration[2] = orientationAngles[2];
    }
}
