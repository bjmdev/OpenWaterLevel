/*
 * Copyright (c) 2019 Bernd Jens Maier. This file is part of OpenWaterLevel.
 *
 * OpenWaterLevel is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * OpenWaterLevel is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with OpenWaterLevel.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package de.maikoe.tools.OpenWaterLevel.ui.scale;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import de.maikoe.tools.OpenWaterLevel.R;

/**
 * This is the main fragment, showing the scale graphics in a custom view. The sensor data for the scale graphics are provided by the ScaleViewModel.
 */
public class ScaleFragment extends Fragment {

    private ScaleViewModel scaleViewModel;
    private SensorManager sensorManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.scale_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sensorManager = (SensorManager) requireActivity().getSystemService(Context.SENSOR_SERVICE);
        scaleViewModel = ViewModelProviders.of(this).get(ScaleViewModel.class);
        ScaleView scaleView = requireActivity().findViewById(R.id.scale_view);
        scaleViewModel.currentAngles.observe(this, scaleView);
        Button calibrateButton = requireActivity().findViewById(R.id.calibrate_button);
        calibrateButton.setOnClickListener(v -> scaleViewModel.calibrate());
    }

    @Override
    public void onResume() {
        super.onResume();
        Sensor gameRotationVectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        if (gameRotationVectorSensor != null) {
            sensorManager.registerListener(scaleViewModel, gameRotationVectorSensor, SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(scaleViewModel);
    }
}
