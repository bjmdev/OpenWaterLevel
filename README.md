# OpenWaterScale
Simple water scale for Android

This app provides a simple water scale for Android:
* Place your smartphone flat on a surface to measure its inclination.
* Press the calibrate button on a level surface to calibrate sensor readings to zero inclination.
* Display is locked into portrait mode and screen is locked to stay on.
* Design is dark, AMOLED friendly.
* Runs on Android 5.0 and above.
